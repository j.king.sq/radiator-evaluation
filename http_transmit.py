import asyncio
from base64 import b16decode as fromhex
from base64 import b64encode as tobase64
import math
import urllib.request
import re

# functions for transmitting firmware over http

# 18 bytes becomes 24 base64 digits without extra data 
chunk_size = 18

def http_send_chunk(host, port, chunk):
    url = "http://{}:{}".format(host, port)
    data = b'CHUNK: ' + tobase64(chunk)
    request = urllib.request.Request(url, data=data, method="POST")
    response = b''
    while response != b'OK\n':
        response = urllib.request.urlopen(request).read()
        

checksum_re = re.compile(r'0x([0-9A-Fa-f]{1,2})')
def http_get_checksum(host, port):
    url = "http://{}:{}".format(host, port)
    request = urllib.request.Request(url, data=b"CHECKSUM", method="POST")
    #TODO try/catch
    response = str(urllib.request.urlopen(request).read())
    checksum_hex = checksum_re.search(response).group(1).upper()

    # server returns minimum number of hex digits, so pad with a '0' where needed
    if len(checksum_hex) == 1:
        checksum_hex = "0" + checksum_hex
    return fromhex(checksum_hex)[0]

async def http_transmit(host, port, data):
    checksum = 0x00
    for i in range(math.ceil(len(data)/chunk_size)):
        chunk_begin = i*chunk_size
        chunk_end = (i+1)*chunk_size
        chunk = data[chunk_begin:chunk_end]
        checksum += sum(chunk)
        checksum &= 0xff
        # send chunk
        http_send_chunk(host, port, chunk)
        # verify checksum
        recipient_checksum = http_get_checksum(host, port)
            
        if recipient_checksum != checksum:
            raise Exception(
                "checksum mismatch: {}, expected {}".format(
                hex(recipient_checksum), hex(checksum)
            ))
