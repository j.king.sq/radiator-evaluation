import asyncio
import csv
from sys import stderr, argv

import hexfile
from http_transmit import http_transmit


# validate command-line args

usage = "Usage: python3 firmware_update.py FIRMWARE_FILE RECIPIENTS_FILE"

if len(argv) == 2 and argv[1] == "--help":
    print(usage)
    exit(0)
if len(argv) != 3:
    stderr.write(usage + "\n")
    exit(1)

firmware_file = argv[1]
recipients_file = argv[2]


# read and validate firmware image

try:
    with open(firmware_file) as hf:
        firmware_image = hexfile.read(hf)
except (IOError, FileNotFoundError) as e:
    stderr.write("{}\n".format(e))
    exit(1)
except hexfile.HexFileException as e:
    stderr.write("Error in {}:\n{}\n".format(firmware_file, e))
    exit(1)


# read recipients file

csv_usage = "recipient list must be of the form:\nprotocol,host,port\n"

hosts = []
tasks = []

try:
    with open(recipients_file) as rf:
        reader = csv.reader(rf)
        for row in reader:
            if len(row) != 3:
                stderr.write("Error on {}:{}\n".format(
                    recipients_file, reader.line_num))
                stderr.write(csv_usage)
                exit(1)

            protocol = row[0]
            host = row[1]
            port = row[2]

            # schedule tasks by protocol
            if protocol == "http":
                tasks.append(http_transmit(host, port, firmware_image))
            #elif protocol == "mqtt": etc.
            else:
                stderr.write("Unknown protocol on {}:{}\n'{}'".format(
                    recipients_file, reader.line_num, protocol))
                for task in tasks:
                    task.close()
                exit(1)

            hosts.append(host)

except (IOError, FileNotFoundError):
    stderr.write("{}\n".format(e))


# transmit to all recipients

async def transmit_all():
    results = await asyncio.gather(return_exceptions=True, *tasks)
    success = True
    for i in range(len(hosts)):
        # check if exceptions ocurred
        if results[i]:
            stderr.write("{}: {}\n".format(hosts[i], results[i]))
            success = False
    return success

if not asyncio.run(transmit_all()):
    stderr.write("\nSome recipients may not have updated correctly!\n")
    exit(1)
