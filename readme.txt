Overview

firmware_update.py has the main program, and should be run with python 3.
It takes two command-line parameters:
    - the hex file for the firmware image
    - a CSV file listing hosts and the protocol/port they each use

The idea is that the firmware could be transferred to each recipient host
concurrently, so that long-running requests or other io operations will not
block.  The transfers share no state and are implemented as coroutines to be
amenable to this design.


Questions

(Updating many devices)
The program transmits data to all hosts listed in the CSV file, which is meant
to demonstrate how many devices could be updated.  A database might become a
better option if host information changed frequently, or if updates needed to be
distributed selectively.

(Asynchronous websocket updates)
I'd add another asynchronous function for this kind of transfer, as well as allow
'websocket' as a protocol that can be listed in the CSV.  It seems like python's
HttpRequest library would be able to handle keeping a connection alive and
buffering response data from the server.  The messages would determine whether
to send the next chunk or resend the current one, making it mostly the same as
the current method.

(Slow response times)
Slow responses on the server side would make it much more important that the
transfers be non-blocking.  If the server would allow it, adding sequence
numbers to the chunks of data would also help.  This way, the server's response
about a given chunk wouldn't delay attempts to transfer proceeding chunks
because any chunk can be re-sent at any time.  TCP uses sequence numbers in this
way for delivering packets.

(Other protocols)
From what I was able to find out about MQTT, it seems like adding MQTT
functionality could be done a few different ways.  I assume that in any
implementation, the devices needing to be updated would be subscribers to a
"firmware" topic.
If there is already a running broker, an MQTT entry in the recipients file would
indicate the hostname or IP, and port, of that broker.  This entry would
correspond to another asynchronous job, which would publish the firmware data to
the firmware topic via a clean session with the broker.

A CoAP implementation seems like it would be very similar to the http
implementation, using POST requests with payloads for the same purposes, only
with smaller chunk sizes.  In addition to re-sending chunks when the devices
respond with an error, there would need to be a reasonable timeout after which
the chunks are assumed not to have been delivered, due to the lack of error
handling that TCP provides.

In the case of a custom TCP protocol, I would define a concise message format
which would map directly to the requests and errors implemented in the http
method of transfer.  As I mentioned earlier, sequence numbers would also be a
nice-to-have for performance.
