from base64 import b16decode as fromhex
from base64 import b64encode as tobase64
import re

class HexFileException(Exception):
    pass

def hexfile_checksum(hex_data):
    return 1 + (sum(fromhex(hex_data)) & 0xff ^ 0xff)

def read(f):
    hex_lines = f.readlines()

    # regex asserting that hex lines contain five or more whole bytes of valid hex

    hex_re = re.compile(r":([0-9A-F]{2}){5,}")

    hex_data = b''
    for i in range(len(hex_lines)):
        line = hex_lines[i]
        if not hex_re.match(line):
            raise HexFileException("Invalid hex on line {}".format(i+1))
        # if record type is 'data', validate and add to image
        if  line[7:9] == "00":

            # read individual fields from this line
            bytecount = fromhex(line[1:3])[0]
            data = fromhex(line[9:-3])
            checksum = fromhex(line[-3:-1])[0]

            # check the size of the data field against the bytecount
            if bytecount != len(data):
                raise HexFileException(
                    "Wrong byte count on line {}\nExpected {} bytes, found {}".format(
                        i+1, bytecount, len(data)))

            # compute checksum of data field and compare with checksum field
            computed_sum = hexfile_checksum(line[1:-3])
            if checksum != computed_sum:
                raise HexFileException(
                    "Wrong checksum on line{}\nExpected {}, actual {}".format(
                        i+1, hex(checksum), hex(computed_sum)))
                    
            hex_data += data
    return hex_data
